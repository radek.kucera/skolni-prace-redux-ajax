import React, { Component } from "react";
import "./App.css";
import { connect } from "react-redux";
import dataActionCreators from "./Stores/Data/DataActions";

class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.sendRequest();
  }

  render() {
    const res = this.props.Data;
    console.log(res);
    const user = res.busy
      ? "Loading..."
      : res.userData.Praha
      ? res.userData.Praha
      : res.userData.Error
      ? res.userData.Error
      : "Chyba";
    return <h1> Praha: {user}</h1>;
  }
}

export default connect(
  state => {
    return {
      Data: state.Data
    };
  },
  {
    sendRequest: dataActionCreators.sendRequest
  }
)(App);
