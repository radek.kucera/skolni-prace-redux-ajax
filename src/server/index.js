const express = require("express");
const jwt = require("jsonwebtoken");

const token = "aee4bd941f8b4d9e39210c06c44fcb71";
const app = express();

app.use(express.static("dist"));

app.get("/api/weather", (req, res) => {
  let key = req.header("Authorization");
  let ver = jwt.verify(key, token);
  if (ver) res.send({ Praha: "sluníčko <3" });
  else res.send({ Error: "Nemáš klíč, sorryako :(" });
});

app.listen(process.env.PORT || 8080, () =>
  console.log(`Listening on port ${process.env.PORT || 8080}!`)
);
